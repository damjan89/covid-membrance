import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/home/Home.vue'
import AboutUsComponent from '@/views/aboutUs/AboutUs.vue'
import DepotComponent from '@/views/depot/Depot.vue'
import ExhibitionComponent from '@/views/exibition/Exhibition.vue'
import LaboratoryComponent from '@/views/laboratory/Laboratory.vue'
import MyStoryComponent from '@/views/myStory/MyStory.vue'
import SlideDetailsComponent from '@/views/slideDetails/SlideDetails.vue'
import CovidDiaryDetails from '@/views/myStory/covidDiaryDetails/CovidDiaryDetails.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/aboutUs',
    name: 'AboutUs',
    component: AboutUsComponent
  },
  {
    path: '/depot',
    name: 'Depot',
    component: DepotComponent
  },
  {
    path: '/exhibition',
    name: 'Exhibition',
    component: ExhibitionComponent
  },
  {
    path: '/events',
    name: 'Laboratory',
    component: LaboratoryComponent
  },
  {
    path: '/myStory',
    name: 'MyStory',
    component: MyStoryComponent
  },
  {
    path: '/slideDetails',
    name: 'SlideDetails',
    component: SlideDetailsComponent,
    props: { slide: null }
  },
  {
    path: '/CovidDiaryDetails',
    name: 'CovidDiaryDetails',
    component: CovidDiaryDetails,
    props: true
  }
]

const router = new VueRouter({
  routes
})

export default router
