import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import '@/assets/css/main.css'
import '@/assets/css/all.css'
import VueI18n from 'vue-i18n'
import * as enTranslations from './i18n/en'
import * as mkTranslations from './i18n/mk'
import vuetify from '@/plugins/vuetify' // path to vuetify export
import VueAnalytics from 'vue-analytics'

Vue.config.productionTip = false
Vue.use(VueI18n)
const messages = {
  en: enTranslations,
  mk: mkTranslations
}
const i18n: object = new VueI18n({
  locale: 'en',
  messages
})
Vue.use(VueAnalytics, {
  id: 'UA-207907191-1',
  router
})
new Vue({
  router,
  i18n,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
