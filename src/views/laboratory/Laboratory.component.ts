import { Component, Vue } from 'vue-property-decorator'
@Component
export default class LaboratoryComponent extends Vue {
  public showMarker: any
  public eventItems: any[]
  public authorized: any
  public api: any
  public selectedEvent: any
  public selectedOpen: boolean
  public selectedElement: any
  public type: string
  public types: string[]
  public mode: string
  public value: string
  public modes: string[]
  public weekday: any[]
  public weekdays: any[]
  constructor () {
    super()
    this.showMarker = true
    this.authorized = false
    this.selectedOpen = false
    this.api = null
    this.selectedEvent = {}
    this.selectedElement = {}
    this.eventItems = []
    this.type = 'month'
    this.types = ['month', 'week', 'day', '4day']
    this.mode = 'stack'
    this.modes = ['stack', 'column']
    this.weekday = [0, 1, 2, 3, 4, 5, 6]
    this.weekdays = [
      { text: 'Sun - Sat', value: [0, 1, 2, 3, 4, 5, 6] },
      { text: 'Mon - Sun', value: [1, 2, 3, 4, 5, 6, 0] },
      { text: 'Mon - Fri', value: [1, 2, 3, 4, 5] },
      { text: 'Mon, Wed, Fri', value: [1, 3, 5] }
    ]
    this.value = ''
  }
}
