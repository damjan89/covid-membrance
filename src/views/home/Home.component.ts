import { Component, Vue } from 'vue-property-decorator'
import MainMenu from '@/components/mainMenu/mainMenu.vue'
import Slide from '../../components/carousel/Slide.vue'
import carousel3d from '../../components/carousel/Carousel3d.vue'
import axios, { AxiosResponse } from 'axios'

@Component({
  components: {
    MainMenu,
    carousel3d,
    Slide
  }
})
export default class HomeComponent extends Vue {
  public showMenu: boolean
  public displayTitleIndex: any
  public displayTitle: boolean
  public slides: any[]

  constructor () {
    super()
    this.showMenu = false
    this.displayTitle = false
    this.slides = []
    this.displayTitleIndex = null
  }

  public mounted () {
    this.getSlides()
  }

  public getSlides () {
    axios.get('./config/slides.json').then((resp: AxiosResponse) => {
      this.slides = resp.data
    })
  }

  public showTitle (item: any) {
    this.displayTitle = true
    this.displayTitleIndex = item.id
  }

  public hideTitle () {
    this.displayTitle = false
    this.displayTitleIndex = null
  }

  public enterSlide (sl: any) {
    const slide = this.slides[sl.index]
    if (slide.private) return
    this.$router.push(`/slideDetails?id=${slide.id}`)
  }
}
