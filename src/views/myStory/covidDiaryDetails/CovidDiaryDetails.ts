import { Component, Vue, Watch } from 'vue-property-decorator'
import axios, { AxiosResponse } from 'axios'

@Component
export default class CovidDiaryDetails extends Vue {
  public details: any
  constructor () {
    super()
    this.details = null
  }

  @Watch('$route', { immediate: true, deep: true })
  public updateSlide (from: any) {
    const id = from.query ? from.query.id : ''
    axios.get('./config/diaryNotes.json').then((resp: AxiosResponse) => {
      this.details = resp.data.find((item: any) => item.id === parseInt(id))
    })
  }

  public goBack () {
    this.$router.push('/myStory?activeTab=1')
  }
}
